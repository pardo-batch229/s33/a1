//3.
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json) => console.log(json));
//4.
fetch("https://jsonplaceholder.typicode.com/todos")
  .then(response => response.json())
  .then(json => {
    let titles = json.map(item => {return item.title});
    console.log(titles);
  })
//5.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=> response.json())
.then((json) => console.log(json));
//6.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=> response.json())
.then(json =>{
  let titles = json.title;
  let complete = json.completed;
  console.log(`The item "${titles}" on the list has a status of ${complete}`);
});
//7.
fetch("https://jsonplaceholder.typicode.com/todos",{
    method: "POST",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        title: "Created To Do List Item",
        completed: false,
        userId: 1,
        id: 201
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

// 8 and 9
fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "PUT",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        id: 1,
        status: "Pending",
        title: "Updated To Do List Item",
        userId: 1
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

// 10 and 11
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method: "PATCH",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "07/09/21",
        status: "Complete"
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

//12
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method: "DELETE"
});